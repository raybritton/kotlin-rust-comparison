#[derive(Default)]
struct AutoDefaults {
    x: i32,
    y: i32,
}

struct ManualDefaults {
    foo: i64,
}

impl ManualDefaults {
    //this is essentially a constructor
    fn new() -> ManualDefaults {
        ManualDefaults { foo: 50 }
    }
}

fn use_defaults() {
    let auto = AutoDefaults::default();
    let partial = AutoDefaults {
        x: 4,
        ..AutoDefaults::default()
    };
    let manual = ManualDefaults::new();
}
