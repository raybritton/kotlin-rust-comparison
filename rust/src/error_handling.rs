fn create_string(length: i32) -> Result<String, &'static str> {
    if length < 0 {
        return Err("Length must be equal to or greater than 0");
    }
    return Ok(format!("{:*^1$}", "", length as usize));
}

pub fn make_strings() {
    match create_string(20) {
        Ok(output) => println!("Long: {}", output),
        Err(e) => eprintln!("Error: {}", e),
    }

    match create_string(-3) {
        Ok(output) => println!("Invalid: {}", output),
        Err(e) => eprintln!("Error: {}", e),
    }
}

fn will_crash() {
    create_string(-1);
}
