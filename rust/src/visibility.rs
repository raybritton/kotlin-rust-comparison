fn this_method_can_only_be_accessed_inside_this_file() {}

pub fn this_method_can_be_accessed_from_anywhere() {}

struct PrivateStruct {}

//Note that private values can't be referenced outside this file, meaning it will be impossible to make instances of this struct without a public method in this file
pub struct PublicStruct {
    private_value: i32,
    pub public_value: i32
}
