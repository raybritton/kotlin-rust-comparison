fn make_list() {
    let type_inferred_list = vec![1, 2, 3];
    let explicit_type_list: Vec<f64> = vec![1.0, 2.0, 3.0];
}

trait NewList<T> {
    fn add(&mut self, value: T);
    fn get(&self, idx: usize) -> Option<&T>;
}

struct NewListImpl<T> {
    internal_values: Vec<T>,
}

impl<T> NewList<T> for NewListImpl<T> {
    fn add(&mut self, value: T) {
        self.internal_values.push(value);
    }

    fn get(&self, idx: usize) -> Option<&T> {
        return self.internal_values.get(idx);
    }
}

fn use_new_list() {
    let mut list = NewListImpl {
        internal_values: vec![],
    };

    list.add("foo");

    list.get(0);
}
