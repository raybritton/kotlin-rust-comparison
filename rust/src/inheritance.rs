trait ParentTrait {
    fn foo();
}

trait ChildTrait: ParentTrait {
    fn bar();
}

struct ImplStruct {}

impl ParentTrait for ImplStruct {
    fn foo() {}
}

struct ImplStruct2 {}

impl ParentTrait for ImplStruct2 {
    fn foo() {}
}

impl ChildTrait for ImplStruct2 {
    fn bar() {}
}

//no equivalent of subclassing with structs
