static nullable_string: Option<&str> = None;

static non_nullable_string: &str = "";

pub fn accessing_optionals() {
    println!("{}", nullable_string.unwrap_or("The string was null"));

    if nullable_string == None {
        println!("Still null");
    }

    if let Some(unwrapped) = nullable_string {
        println!("Only runs if not null {}", unwrapped);
    }
}

fn will_crash() {
    println!("{}", nullable_string.unwrap());
}
