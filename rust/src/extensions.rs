trait Foo {}

trait Ext {
    fn ext();
}

impl Ext for Foo {
    fn ext() {}
}

struct Bar {}

impl Ext for Bar {
    fn ext() {}
}
