mod constants;
mod defaults;
mod error_handling;
mod extensions;
mod formatting;
mod generics;
mod inheritance;
mod lambdas;
mod optionals;
mod visibility;

use crate::constants::*;
use crate::error_handling::make_strings;
use crate::formatting::*;
use crate::lambdas::method_runner;
use crate::optionals::accessing_optionals;

fn main() {
    println!("Rust language demo");
    println!(
        "This demo shows {} features in {} files.",
        FEATURE_COUNT, FILE_COUNT
    );

    accessing_optionals();
    formatting();
    debug();
    make_strings();
    method_runner();
}
