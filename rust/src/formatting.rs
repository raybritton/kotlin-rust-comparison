use crate::constants::FEATURE_COUNT;

static mutliline_string: &str = r#"
Anything can be written
in this string (e.g. ") and it
doesn't need to escaped
"#;

pub fn formatting() {
    println!("This line ends with a variable: {}", FEATURE_COUNT);
    let string = format!("This is another approach: {}", FEATURE_COUNT);
    println!("{}", string);
}

#[derive(Debug)]
struct Point {
    x: i32,
    y: i32,
}

pub fn debug() {
    let point = Point { x: 1, y: 1 };
    println!("{:?}", point);
    println!("{:#?}", point);
    dbg!(point);
}
