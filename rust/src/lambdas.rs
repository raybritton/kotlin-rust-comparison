static method_ref: fn(i32) -> i32 = { |num| num * 2 };

fn higher_order(block: fn() -> i32) {
    println!("Value from block: {}", block());
}

fn method_builder(mode: usize) -> fn(&str) -> String {
    match mode {
        0 => return { |input| input.chars().rev().collect() },
        1 => return { |input| input.to_uppercase() },
        _ => panic!("Invalid value"),
    }
}

pub fn method_runner() {
    higher_order({ || 4 });
    println!("{}", method_builder(0)("HELLO"));
    println!("{}", method_builder(1)("world"));
}
