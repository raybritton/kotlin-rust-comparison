fun makeList() {
    val typeInferredList = listOf(1, 2, 3)
    val explicitTypeList: List<Double> = listOf(1.0, 2.0, 3.0)
}

interface NewList<T> {
    fun add(value: T)
    fun get(idx: Int): T?
}

class NewListImpl<T> : NewList<T> {
    private val internalValues = mutableListOf<T>()

    override fun add(value: T) {
        internalValues.add(value)
    }

    override fun get(idx: Int): T? {
        return internalValues.getOrNull(idx)
    }
}

fun useNewList() {
    val list = NewListImpl<String>()

    list.add("foo")

    list.get(0)
}
