import kotlin.IllegalArgumentException

fun createString(length: Int): String {
    if (length < 0) {
        throw IllegalArgumentException("Length must be equal to or greater than 0")
    }
    return "".padStart(length, '*')
}

fun makeStrings() {
    try {
        println("Long:" + createString(20))
    } catch (e: IllegalArgumentException) {
        e.printStackTrace()
    }

    try {
        println("Invalid:" + createString(-3))
    } catch (e: IllegalArgumentException) {
        e.printStackTrace()
    }
}

private fun willCrash() {
    createString(-1)
}