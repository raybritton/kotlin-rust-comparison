fun main() {
    println("Kotlin language demo")
    println("This demo shows $FEATURE_COUNT features in $FILE_COUNT files.")

    accessingOptionals()
    formatting()
    debug()
    makeStrings()
    methodRunner()
}