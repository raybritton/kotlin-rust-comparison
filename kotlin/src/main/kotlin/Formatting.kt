val mutlilineString = """
    Anything can be written
    in this string (e.g. ") and it
    doesn't need to escaped
""".trimIndent()

fun formatting() {
    println("This line ends with a variable: $FEATURE_COUNT")
    val str = String.format("This is another approach: %d", FEATURE_COUNT)
    println(str)
}

data class Point(
    val x: Int,
    val y: Int
)

fun debug() {
    println(Point(1, 1).toString())
}

