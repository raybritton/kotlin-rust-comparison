val nullableString: String? = null

val nonNullableString: String = ""

fun accessingOptionals() {
    println(nullableString ?: "The string was null")
    if (nullableString == null) {
        println("Still null")
    }
    nullableString?.let {
        println("Only runs if not null: $it")
    }
}

private fun willCrash() {
    print(nullableString!!)
}