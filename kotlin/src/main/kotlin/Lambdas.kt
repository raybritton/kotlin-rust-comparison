import java.lang.IllegalArgumentException

val methodRef: (Int) -> Int = { it * 2 }

fun higherOrder(block: () -> Int) {
    println("Value from block: ${block()}")
}

fun methodBuilder(mode: Int): (String) -> String {
    return when (mode) {
        0 -> { str -> str.reversed() }
        1 -> { str -> str.toUpperCase() }
        else -> throw IllegalArgumentException("Invalid value")
    }
}

fun methodRunner() {
    higherOrder { 4 }
    println(methodBuilder(0)("HELLO"))
    println(methodBuilder(1)("world"))
}