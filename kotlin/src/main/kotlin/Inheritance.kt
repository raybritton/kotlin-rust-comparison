interface ParentInterface {
    fun foo()
}

interface ChildInterface : ParentInterface {
    fun bar()
}

class ImplClass: ParentInterface {
    override fun foo() {}
}

class ImplClass2: ChildInterface {
    override fun foo() {}

    override fun bar() {}
}

open class ParentClass {
    fun foobar() {

    }
}

class ChildClass: ParentClass() {
    fun foobaz() {
        foobar()
    }
}

