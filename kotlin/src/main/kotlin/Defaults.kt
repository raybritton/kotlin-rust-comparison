//no equivalent for auto defaults

data class ManualDefaults(
    val foo: Long = 50
)

fun useDefaults() {
    val manual = ManualDefaults();
}