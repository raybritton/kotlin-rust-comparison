private fun thisMethodCanOnlyBeAccessedInsideThisFile() {}

fun thisMethodCanBeAccessedFromAnywhere() {}

private class PrivateClass {}

class PublicClass {}